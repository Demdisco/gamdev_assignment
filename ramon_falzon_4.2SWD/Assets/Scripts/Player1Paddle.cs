﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player1Paddle : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	

    void Update()
    {
        transform.Translate(Vector3.up * Input.GetAxis("Vertical") * 25f * Time.deltaTime);
        transform.position = new Vector3(-8.2f, Mathf.Clamp(transform.position.y, -4.2f, 4.2f));
    }
}
