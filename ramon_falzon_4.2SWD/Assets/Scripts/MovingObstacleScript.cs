﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObstacleScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.down * 5f * Time.deltaTime);
        if (transform.position.y < -6f)
        {
            transform.position = new Vector3(transform.position.x, 6f);
        }
	}
}
