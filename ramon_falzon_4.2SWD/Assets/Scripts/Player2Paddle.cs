﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Paddle : MonoBehaviour {
    
    void Update()
    {
        Vector3 mousePosition = Input.mousePosition;
        Vector3 paddlePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        transform.Translate(Vector3.up * Input.GetAxis("Vertical") * 25f * Time.deltaTime);
        transform.position = new Vector3(8.2f, Mathf.Clamp(paddlePosition.y, -4.2f, 4.2f));
    }
}
