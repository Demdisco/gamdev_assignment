﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ButtonScripts : MonoBehaviour {

    public void toMainMenuClicked()
    {
        SceneManager.LoadScene("mainMenu");
    }

    public void toLevel1Clicked()
    {
        SceneManager.LoadScene("Level1");
    }

    public void exitClicked()
    {
        Application.Quit();
    }

}
