﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalScreenControllerScript : MonoBehaviour {
    GameObject scoreKeeper;
	// Use this for initialization
	void Start () {
        scoreKeeper = GameObject.Find("ScoreKeeper");
        int p1TotalScore = scoreKeeper.GetComponent<ScoreKeeperScript>().p1TotalScore;
        int p2TotalScore = scoreKeeper.GetComponent<ScoreKeeperScript>().p2TotalScore;

        GameObject.Find("P1Score").GetComponent<Text>().text = p1TotalScore.ToString();
        GameObject.Find("P2Score").GetComponent<Text>().text = p2TotalScore.ToString();

        if (p1TotalScore > p2TotalScore)
        {
            gameObject.GetComponent<Text>().text = "Player 1 wins!\nTHE DARK SIDE HAS BEEN DEFEATED";

        } else if (p2TotalScore > p1TotalScore)
        {
            gameObject.GetComponent<Text>().text = "Player 2 wins!\nTHE DARK SIDE HAS WON";
        } else
        {
            gameObject.GetComponent<Text>().text = "Draw!";
        }
        Destroy(scoreKeeper);
    }
}
