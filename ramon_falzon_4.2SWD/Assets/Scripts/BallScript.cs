﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BallScript : MonoBehaviour {

    public int p1Score = 0;
    public int p2Score = 0;
    public int currLevel;
    public float ballSpeed;
    int randomnum;
    GameObject scoreKeeper;
    GameObject timerText;
    // Use this for initialization
    void Start () {
        scoreKeeper = GameObject.Find("ScoreKeeper");
        timerText = GameObject.Find("countdownText");
        BallReset();
    }

    void BallReset()
    {
        gameObject.transform.position = new Vector3(0, 0);
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0);
        StartCoroutine(StartBall());
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Paddle1")
        {
            gameObject.GetComponent<Rigidbody2D>().velocity += new Vector2(0, Random.Range(-1f,1f));
        }

        if (collision.gameObject.name == "Paddle2")
        {
            gameObject.GetComponent<Rigidbody2D>().velocity += new Vector2(0, Random.Range(1f, 1f));
        }

        if (collision.gameObject.name == "P1Lose")
        {
            p2Score++;
            GameObject.Find("P2Score").GetComponent<Text>().text = p2Score.ToString();
            BallReset();
            CheckWinner();
        }

        if (collision.gameObject.name == "P2Lose")
        {
            p1Score++;
            GameObject.Find("P1Score").GetComponent<Text>().text = p1Score.ToString();
            BallReset();
            CheckWinner();
        }
    }
    
    void CheckWinner()
    {
        switch (currLevel)
        {
            case 1:
                scoreKeeper.GetComponent<ScoreKeeperScript>().AddScores(p1Score, p2Score);
                SceneManager.LoadScene("Level2");
                break;
            case 2:
                if (p1Score >= 3 || p2Score >= 3)
                {
                    scoreKeeper.GetComponent<ScoreKeeperScript>().AddScores(p1Score, p2Score);
                    SceneManager.LoadScene("Level3");
                }
                break;
            case 3:
                if (p1Score >= 4 || p2Score >= 4)
                {
                    scoreKeeper.GetComponent<ScoreKeeperScript>().AddScores(p1Score, p2Score);
                    SceneManager.LoadScene("GameEnd");
                }
                break;

        }
    }

    IEnumerator StartBall()
    {
        timerText.GetComponent<Text>().CrossFadeAlpha(255f, 0f, false);
        timerText.GetComponent<Text>().text = "3";
        yield return new WaitForSeconds(1);
        timerText.GetComponent<Text>().text = "2";
        yield return new WaitForSeconds(1);
        timerText.GetComponent<Text>().text = "1";
        yield return new WaitForSeconds(1);

        timerText.GetComponent<Text>().CrossFadeAlpha(0f, 0f, false);
        randomnum = Random.Range(0, 2);
        switch (randomnum)
        {
            case 0:
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(ballSpeed, Random.Range(-1f, 1f));
                break;
            case 1:
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(ballSpeed * -1, Random.Range(-1f, 1f));
                break;
        }
    }
}
