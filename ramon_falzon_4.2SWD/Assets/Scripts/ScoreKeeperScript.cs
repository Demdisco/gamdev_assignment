﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreKeeperScript : MonoBehaviour {

    public int p1TotalScore = 0;
    public int p2TotalScore = 0;

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(this);
	}

    public void AddScores(int p1, int p2)
    {
        p1TotalScore += p1;
        p2TotalScore += p2;
    }
}
